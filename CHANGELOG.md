# [1.2.0](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/compare/v1.1.0...v1.2.0) (2024-06-24)


### Features

* add job title, company name and roles ([86b1b22](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/86b1b221fef876eec3e845dde454ed7f8f3ba876))

# [1.1.0](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/compare/v1.0.0...v1.1.0) (2024-05-30)


### Bug Fixes

* add missing env variable ([f2ca914](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/f2ca91492fc2ce11b3d96612fd7234125ed044d4))


### Features

* add token authentication for code authorization flow from frontend ([dfc64b3](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/dfc64b3d2d6d5e557c69c36d01c6c0201f837138))
* add vcard and schema.org credential types ([ae5cb3e](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/ae5cb3e27520fda642e11521b041acf07f0cfc6c))

# 1.0.0 (2024-05-21)


### Bug Fixes

* add missing credential type ([014e1fe](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/014e1fed31656ebd2c7e410ce3418d78dbd243e8))
* change node version in semantic release job ([f5abb9f](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/f5abb9f3eafad6bfcfd6fe90dc4bf3ef1823a975))


### Features

* sign a gaia-x membership credential ([23df531](https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/commit/23df531756c750e6ebf2fed34d278a76978368da))
