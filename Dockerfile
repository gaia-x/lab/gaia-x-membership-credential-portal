FROM node:lts-alpine AS builder

WORKDIR /usr/src/app
COPY . .
RUN npm install && npm run build

FROM node:lts-alpine
WORKDIR /usr/app

COPY --from=builder /usr/src/app/node_modules node_modules
COPY --from=builder /usr/src/app/dist dist

CMD ["node", "dist/src/main"]
EXPOSE 3000
