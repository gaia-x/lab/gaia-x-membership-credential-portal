# Gaia-X Membership Credential Portal

This project scope is to issue [Verifiable Credential](https://www.w3.org/TR/vc-data-model-2.0/) for Gaia-X members to
demonstrate their membership in a self sovereign identity manner. 

This project uses the authentication from the [Membership Platform](https://membersplatform.gaia-x.eu/) to ensure a user is a Gaia-X member.

## Using the API

In order to obtain a Verifiable Credential for Gaia-X membership, you only need to access any deployment with the endpoint `/membership`

## Deployments

// TODO add after deployment

## Running locally

Please use a Node.js LTS version and install the required dependencies with the following commands.

```bash
nvm install
npm install
```

```bash
cp .env.example .env
```

In order to run this project you need to complete the `.env` with the following variables:
- PRIVATE_KEY
- X509_CERTIFICATE
- AUTH_CLIENT_ID
- AUTH_CLIENT_SECRET

```bash
npm run start:dev
```

## Running Tests

Tests are made with the Jest framework.
To run tests, run the following command.

```bash
  npm run test
```
