import { ConfigService } from '@nestjs/config'

import { AppController } from './app.controller'

describe('AppController', () => {
  it.each([undefined, null, '', ' '])('should throw and error when base URL is missing', (baseUrl: string) => {
    const configService: ConfigService = new ConfigService()
    jest.spyOn(configService, 'get').mockReturnValue(baseUrl)

    expect(() => new AppController(configService)).toThrow(`Please provide this service's base url through the BASE_URL environment variable`)
  })
})
