import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, UseInterceptors } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'

import * as packageJson from '../package.json'

@ApiTags('gx-membership-credential')
@Controller()
@UseInterceptors(CacheInterceptor)
export class AppController {
  private readonly baseUrl: string

  constructor(private readonly configService: ConfigService) {
    this.baseUrl = this.configService.get<string>('BASE_URL')?.trim()

    if (!this.baseUrl) {
      throw new Error(`Please provide this service's base url through the BASE_URL environment variable`)
    }
  }

  @Get()
  @Header('Content-Type', 'application/json')
  @ApiOkResponse({
    description: `Gaia-X Membership Credential application information`,
    content: {
      'application/json': {
        example: {
          software: 'gaia-x-membership-credential',
          description: 'Gaia-X Membership Credential API service',
          version: '1.0.0',
          documentation: 'https://membership.lab.gaia-x.eu/development/docs/',
          repository: {
            type: 'git',
            url: 'git@gitlab.com:gaia-x/lab/gaia-x-membership-credential-portal.git'
          },
          bugs: {
            url: 'https://gitlab.com/gaia-x/lab/gaia-x-membership-credential-portal/-/issues'
          }
        }
      }
    }
  })
  getApplicationInformation() {
    return {
      software: packageJson.name,
      description: packageJson.description,
      version: packageJson.version,
      documentation: `${this.baseUrl}/docs/`,
      repository: packageJson.repository.url,
      bugs: packageJson.bugs.url
    }
  }
}
