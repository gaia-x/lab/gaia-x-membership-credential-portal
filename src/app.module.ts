import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AppController } from './app.controller'
import { AuthModule } from './auth/auth.module'
import { IdentityModule } from './identity/identity.module'
import { MembershipCredentialModule } from './membership-credential/membership-credential.module'

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register({
      isGlobal: true
    }),
    IdentityModule,
    AuthModule,
    MembershipCredentialModule
  ],
  controllers: [AppController]
})
export class AppModule {}
