import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { PassportModule } from '@nestjs/passport'

import { AuthClientProvider } from './service/auth-client.provider'
import { AuthenticationService } from './service/authentication.service'
import { OidcStrategyService } from './service/oidc-strategy.service'
import { SessionSerializer } from './service/session.serializer'


@Module({
  imports: [ConfigModule, PassportModule.register({ session: true, defaultStrategy: 'oidc' }), HttpModule],
  providers: [AuthClientProvider.create(), OidcStrategyService, SessionSerializer, AuthenticationService],
  exports: [AuthenticationService]
})
export class AuthModule {}
