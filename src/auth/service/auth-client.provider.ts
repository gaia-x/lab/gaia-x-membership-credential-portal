import { Provider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { BaseClient, Issuer } from 'openid-client'

export class AuthClientProvider {
  static create(): Provider {
    return {
      inject: [ConfigService],
      provide: 'authClient',
      useClass: BaseClient,
      useFactory: async (configService: ConfigService): Promise<BaseClient> => {
        const TrustIssuer: Issuer<BaseClient> = await Issuer.discover(configService.get<string>('AUTH_CONFIGURATION_URL'))
        return new TrustIssuer.Client({
          client_id: configService.get<string>('AUTH_CLIENT_ID'),
          client_secret: configService.get<string>('AUTH_CLIENT_SECRET')
        })
      }
    }
  }
}
