import { HttpService } from '@nestjs/axios'
import { UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { KeycloakUserDto } from '../../membership-credential/dto/keycloak-user.dto'
import { AuthenticationService } from './authentication.service'

jest.mock('@nestjs/axios')
jest.mock('@nestjs/config')

jest.mock('rxjs', () => ({
  lastValueFrom: jest.fn().mockImplementation(value => {
    return value
  })
}))

describe('AuthenticationService', () => {
  let authenticationService: AuthenticationService
  let httpService: jest.Mocked<HttpService>
  let configService: ConfigService
  const someUser: KeycloakUserDto = {
    name: 'name',
    email: 'email',
    sub: 'sub',
    isGaiaXMember: true,
    job_title: 'Software Engineer',
    associated_company_name: 'Gaia-X',
    roles: []
  }

  beforeEach(() => {
    httpService = new HttpService() as jest.Mocked<HttpService>
    configService = new ConfigService()
    jest.spyOn(configService, 'get').mockReturnValue('authBaseUrl')
    authenticationService = new AuthenticationService(httpService, configService)
  })

  it('should authenticate successfully with valid access token', async () => {
    const accessToken = 'valid_access_token'

    ;(httpService.get as jest.Mock).mockReturnValue({ data: someUser })

    const result = await authenticationService.authenticate(accessToken)

    expect(httpService.get).toHaveBeenCalledWith(expect.any(String), {
      headers: { authorization: `Bearer ${accessToken}` }
    })
    expect(result).toEqual(someUser)
  })

  it('should throw UnauthorizedException on HTTP request error', async () => {
    const accessToken = 'invalid_access_token'

    ;(httpService.get as jest.Mock).mockRejectedValue({ error: 'invalidToken' })

    await expect(authenticationService.authenticate(accessToken)).rejects.toThrow(UnauthorizedException)
    expect(httpService.get).toHaveBeenCalledWith(expect.any(String), {
      headers: { authorization: `Bearer ${accessToken}` }
    })
  })
})
