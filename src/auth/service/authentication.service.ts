import { HttpService } from '@nestjs/axios'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { lastValueFrom } from 'rxjs'

import { KeycloakUserDto } from '../../membership-credential/dto/keycloak-user.dto'

@Injectable()
export class AuthenticationService {
  private readonly keycloakBaseUrl: string

  constructor(
    private httpService: HttpService,
    private readonly configService: ConfigService
  ) {
    this.keycloakBaseUrl = configService.get<string>('KEYCLOAK_BASE_URL')?.trim()
  }

  async authenticate(accessToken: string): Promise<KeycloakUserDto> {
    const url = `${this.keycloakBaseUrl}/protocol/openid-connect/userinfo`

    try {
      const response = await lastValueFrom(
        this.httpService.get<KeycloakUserDto>(url, {
          headers: {
            authorization: `Bearer ${accessToken}`
          }
        })
      )

      return response.data
    } catch (e) {
      throw new UnauthorizedException()
    }
  }
}
