import { Inject, Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import { BaseClient, Client, Strategy, TokenSet, UserinfoResponse } from 'openid-client'

import { extractTokenRoles, isGaiaXMember } from '../../utils/token-utils'

@Injectable()
export class OidcStrategyService extends PassportStrategy(Strategy, 'oidc') {
  client: Client

  constructor(
    @Inject('authClient') private readonly authClient: BaseClient,
    private readonly configService: ConfigService
  ) {
    super({
      client: authClient,
      params: {
        redirect_uri: configService.get<string>('BASE_URL')?.trim() + '/callback',
        scope: configService.get<string>('AUTH_SCOPE')
      },
      passReqToCallback: false,
      usePKCE: false,
      callback: true
    })

    this.client = authClient
  }

  // Required passport method
  async validate(tokenSet: TokenSet) {
    const userinfo: UserinfoResponse = await this.client.userinfo(tokenSet)
    userinfo.isGaiaXMember = isGaiaXMember(tokenSet.access_token)
    userinfo.roles = extractTokenRoles(tokenSet.access_token)
    try {
      return {
        userinfo,
        ...tokenSet
      }
    } catch (err) {
      throw new UnauthorizedException(err)
    }
  }
}
