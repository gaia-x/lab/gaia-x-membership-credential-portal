import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, UseInterceptors } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { DidService } from '../service/did.service'

const didExample = {
  '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
  id: 'did:web:membership.lab.gaia-x.eu:development',
  verificationMethod: [
    {
      '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
      id: 'did:web:membership.lab.gaia-x.eu:development#X509-JWK2020',
      type: 'JsonWebKey2020',
      controller: 'did:web:membership.lab.gaia-x.eu:development',
      publicKeyJwk: {
        kty: 'RSA',
        n: 'ulmXEa0nehbR338h6QaWLjMqfXE7mKA9PXoC_6_8d26xKQuBKAXa5k0uHhzQfNlAlxO-IpCDgf9cVzxIP-tkkefsjrXc8uvkdKNK6TY9kUxgUnOviiOLpHe88FB5dMTH6KUUGkjiPfq3P0F9fXHDEoQkGSpWui7eD897qSEdXFre_086ns3I8hSVCxoxlW9guXa_sRISIawCKT4UA3ZUKYyjtu0xRy7mRxNFh2wH0iSTQfqf4DWUUThX3S-jeRCRxqOGQdQlZoHym2pynJ1IYiiIOMO9L2IQrQl35kx94LGHiF8r8CRpLrgYXTVd9U17-nglrUmJmryECxW-555ppQ',
        e: 'AQAB',
        alg: 'PS256',
        x5u: 'https://membership.lab.gaia-x.eu/development/.well-known/x509CertificateChain.pem'
      }
    }
  ],
  assertionMethod: ['did:web:membership.lab.gaia-x.eu:development#X509-JWK2020']
}

@ApiTags('did')
@Controller()
@UseInterceptors(CacheInterceptor)
export class DidController {
  constructor(private readonly didService: DidService) {}

  @Get(['did.json', '.well-known/did.json'])
  @ApiOperation({
    summary: 'Collect the Gaia-X Membership Credential DID'
  })
  @Header('Content-Type', 'application/json')
  @ApiOkResponse({
    description: `The Gaia-X Membership Credential's Decentralized Identifier (DID)`,
    content: {
      'application/json': {
        example: didExample
      }
    }
  })
  getDid(): Promise<string> {
    return this.didService.buildDid()
  }
}
