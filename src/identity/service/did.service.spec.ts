import { ConfigService } from '@nestjs/config'
import { mkdtempSync, writeFileSync } from 'fs'
import { pki } from 'node-forge'
import { tmpdir } from 'os'
import { join } from 'path'

import { CertificateBuilder } from '../../utils/certificate-builder'
import { KeyBuilder } from '../../utils/key-builder'
import { DidService } from './did.service'

describe('DidService', () => {
  const keyPair: pki.KeyPair = KeyBuilder.buildKeyPair()

  let configService: ConfigService
  let didService: DidService
  let certificate: pki.Certificate

  beforeEach(() => {
    configService = new ConfigService()
    jest.spyOn(configService, 'get').mockImplementation((property: string) => {
      if (property === 'X509_CERTIFICATE') {
        return certificate?.toString()
      }

      if (property === 'BASE_URL') {
        return 'http://localhost:3000/main'
      }

      throw new Error('Invalid configuration property')
    })

    didService = new DidService(configService)
  })

  it('should get the certificate chain from environment variables', async () => {
    certificate = CertificateBuilder.createCertificate(keyPair)

    expect(didService.getCertChain(null)).toEqual(certificate.toString())
  })

  it('should get the certificate chain from file system', async () => {
    certificate = null
    const fsCertificate: pki.Certificate = CertificateBuilder.createCertificate(keyPair)

    const certificatePath = mkdtempSync(join(tmpdir(), 'membership-credential-certificate-')) + '/certificate.pem'
    writeFileSync(certificatePath, fsCertificate.toString())

    expect(didService.getCertChain(certificatePath)).toEqual(fsCertificate.toString())
  })
})
