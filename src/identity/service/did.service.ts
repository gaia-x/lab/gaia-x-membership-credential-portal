import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { readFileSync } from 'fs'
import * as jose from 'jose'
import { join } from 'path'

import { DidUtils } from '../../utils/did.utils'

@Injectable()
export class DidService {
  static readonly FALLBACK_CERTIFICATE_PATH = join(__dirname, '../static/.well-known/x509CertificateChain.pem')
  private readonly publicKeyAlgorithm: string = 'PS256'
  private readonly x509VerificationMethod: string = 'X509-JWK2020'
  private readonly baseUrl: string
  private readonly didWeb: string

  constructor(private readonly configService: ConfigService) {
    this.baseUrl = this.configService.get<string>('BASE_URL').trim()
    this.didWeb = DidUtils.buildDidWebFromBaseUrl(this.baseUrl)
  }

  getCertChain(fallbackPath: string): string {
    return this.configService.get<string>('X509_CERTIFICATE') || readFileSync(fallbackPath).toString()
  }

  async buildDid(): Promise<any> {
    const certChainUri = `${this.baseUrl}/.well-known/x509CertificateChain.pem`

    const certChain = await jose.importX509(this.getCertChain(DidService.FALLBACK_CERTIFICATE_PATH), this.publicKeyAlgorithm)
    const x509VerificationMethodIdentifier = `${this.didWeb}#${this.x509VerificationMethod}`

    return {
      '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
      id: this.didWeb,
      verificationMethod: [
        {
          '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
          id: x509VerificationMethodIdentifier,
          type: 'JsonWebKey2020',
          controller: this.didWeb,
          publicKeyJwk: {
            ...(await jose.exportJWK(certChain)),
            alg: this.publicKeyAlgorithm,
            x5u: certChainUri
          }
        }
      ],
      assertionMethod: [x509VerificationMethodIdentifier]
    }
  }
}
