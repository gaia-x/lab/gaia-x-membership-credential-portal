import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import session from 'express-session'
import passport from 'passport'

import * as packageJson from '../package.json'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const configService = app.get(ConfigService)

  const globalPrefix = configService.get<string>('APP_PATH')?.trim() ?? ''
  app.setGlobalPrefix(globalPrefix)
  app.enableCors()
  app.use(
    session({
      store: new session.MemoryStore(),
      secret: configService.get<string>('SESSION_SECRET'),
      resave: false,
      saveUninitialized: false,
      rolling: true,
      cookie: {
        maxAge: 30 * 60 * 1000,
        httpOnly: true
      }
    })
  )
  app.use(passport.initialize())
  app.use(passport.session())

  const config = new DocumentBuilder()
    .addServer(globalPrefix)
    .setTitle('Membership Portal')
    .setDescription('Gaia-X Membership Portal')
    .setVersion(packageJson.version)
    .addTag('gx-membership-credential')
    .build()
  const document = SwaggerModule.createDocument(app, config, {
    ignoreGlobalPrefix: true
  })
  SwaggerModule.setup(`${globalPrefix}/docs`, app, document)

  await app.listen(3000)
}

bootstrap()
