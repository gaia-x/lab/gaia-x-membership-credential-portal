import { VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { Controller, ForbiddenException, Get, Header, Query, Req, Res, UseGuards } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { MembershipCredentialTypes } from '../dto/membership-credential-types'
import { AuthenticatedGuard } from '../guard/authenticated.guard'
import { AuthenticatedOrTokenGuard } from '../guard/authenticatedOrToken.guard'
import { MembershipCredentialService } from '../service/membership-credential.service'

const vcExample = {
  '@context': ['https://www.w3.org/2018/credentials/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
  type: ['VerifiableCredential', 'org:ProgramMembership'],
  id: 'did:web:gaia-x.eu:main:da98548c-2b35-48a7-9f22-030ac68131b0',
  issuer: 'did:web:gaia-x.eu:main',
  issuanceDate: '2024-05-07T10:38:50.837Z',
  credentialSubject: {
    type: 'org:ProgramMembership',
    'org:membershipNumber': '015c51cb-185b-43e1-87f2-5f1cb0ae90ec',
    'org:hostingOrganization': 'Gaia-X European Association for Data and Cloud AISBL',
    'org:member': {
      type: 'org:Person',
      'org:name': 'John Doe',
      'org:email': 'john.doe@gaia-x.eu'
    }
  },
  proof: {
    type: 'JsonWebSignature2020',
    created: '2024-05-07T12:38:50.837+02:00',
    proofPurpose: 'assertionMethod',
    verificationMethod: 'did:web:gaia-x.eu:main#X509-JWK2020',
    jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..MY9Z2BTxDu0n-rMqbPKLf15qByZGifi8-wHCs38FaBfyPi9HFyX8ldsgtaT8LxhrXhhIklUtxMqhpuPxQAFiUklYTYSj8vCuGw4yK__Ji-dhZpCHW031qK7zJk8B2utW8SnKXuzyMvVxbwSmgY8fmgwU_-XzHb1DRwX8yJYS9NCtZTqio3s3NsbjDdD3pk6TkvcCQNfrI3RRW05t-ZCNvOY6yVfDS0CPmh7w2KzwGC5ygxMNbx2SI6Wx5uWeIU-5GYEjazbOjcpXVWimJwQ7iu8RbtzBSnGAIrQw35KTNOt7nm5RPoKUQjuYm_9BpnTen1-RV0uF2GV8zjnHh09VEw'
  }
}

@ApiTags('vc')
@Controller()
export class MembershipCredentialController {
  constructor(private membershipCredentialService: MembershipCredentialService) {}

  @UseGuards(AuthenticatedGuard)
  @Get('/callback')
  callback(@Res() res) {
    res.redirect('membership-credential')
  }

  @Get('membership-credential')
  @ApiOperation({
    summary: 'Collect the Gaia-X Membership credential'
  })
  @Header('Content-Type', 'application/ld+json')
  @UseGuards(AuthenticatedOrTokenGuard)
  @ApiOkResponse({
    description: `A Gaia-X membership credential`,
    content: {
      'application/ld+json': {
        example: vcExample
      }
    }
  })
  getMembershipCredential(@Req() req, @Query('type') type: MembershipCredentialTypes): Promise<VerifiableCredential> {
    const user: KeycloakUserDto = req.session.req.user.userinfo || req.user
    if (!user.isGaiaXMember) {
      throw new ForbiddenException('You need to be a Gaia-X member to get this Verifiable Credential')
    }
    return this.membershipCredentialService.buildMembershipCredential(user, type || MembershipCredentialTypes.VCARD)
  }
}
