export class KeycloakUserDto {
  sub: string

  name: string

  email: string

  job_title: string

  associated_company_name: string

  preferred_username?: string

  given_name?: string

  family_name?: string

  email_verified?: boolean

  isGaiaXMember?: boolean

  roles: string[]
}
