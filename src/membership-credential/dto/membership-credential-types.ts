export enum MembershipCredentialTypes {
  VCARD = 'VCARD',
  SCHEMA_ORG = 'SCHEMA_ORG'
}
