import { Proof } from '@gaia-x/json-web-signature-2020'
import { ApiProperty } from '@nestjs/swagger'

import { OrgMembershipCredentialSubjectDto } from './org-membership-credential-subject.dto'
import { VcardMembershipCredentialSubjectDto } from './vcard-membership-credential-subject.dto'


export class MembershipCredentialDto {
  @ApiProperty()
  '@context': string[]

  @ApiProperty()
  type: string[]

  @ApiProperty()
  id: string

  @ApiProperty()
  issuer: string

  @ApiProperty()
  issuanceDate: string

  @ApiProperty()
  credentialSubject: VcardMembershipCredentialSubjectDto | OrgMembershipCredentialSubjectDto

  @ApiProperty()
  proof?: Proof
}
