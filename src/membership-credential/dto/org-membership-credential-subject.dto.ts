import { ApiProperty } from '@nestjs/swagger'

export class OrgMembershipCredentialSubjectDto {
  @ApiProperty()
  type: 'org:ProgramMembership' | string

  @ApiProperty()
  'org:hostingOrganization': string

  @ApiProperty()
  'org:membershipNumber': string

  @ApiProperty()
  'org:member': OrgMember
}

export class OrgMember {
  @ApiProperty()
  type: 'org:Person' | string

  @ApiProperty()
  'org:name': string

  @ApiProperty()
  'org:email': string
}
