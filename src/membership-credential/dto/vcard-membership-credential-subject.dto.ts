import { ApiProperty } from '@nestjs/swagger'

export class VcardMembershipCredentialSubjectDto {
  @ApiProperty()
  type: 'vcard:Organization' | string

  @ApiProperty()
  'vcard:title': string

  @ApiProperty()
  'vcard:hasMember': VcardMember
}

export class VcardMember {
  @ApiProperty()
  id: string

  @ApiProperty()
  type: 'vcard:Individual' | string

  @ApiProperty()
  'vcard:fn': string

  @ApiProperty()
  'vcard:hasEmail': string
}
