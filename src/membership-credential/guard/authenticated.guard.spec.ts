import { ExecutionContext } from '@nestjs/common'

import { AuthenticatedGuard } from './authenticated.guard'


describe('AuthenticatedGuard', () => {
  let authenticatedGuard: AuthenticatedGuard
  let context: jest.Mocked<ExecutionContext>
  let request: any

  beforeEach(() => {
    authenticatedGuard = new AuthenticatedGuard()

    request = {
      isAuthenticated: jest.fn()
    }

    context = {
      switchToHttp: jest.fn().mockReturnValue({
        getRequest: jest.fn().mockReturnValue(request)
      })
    } as unknown as jest.Mocked<ExecutionContext>
  })

  it('should return true if the request is authenticated', async () => {
    request.isAuthenticated.mockReturnValue(true)

    const result = await authenticatedGuard.canActivate(context)

    expect(request.isAuthenticated).toHaveBeenCalled()
    expect(result).toBe(true)
  })
})
