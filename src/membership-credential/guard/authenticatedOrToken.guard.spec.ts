import { ExecutionContext } from '@nestjs/common'

import { AuthenticationService } from '../../auth/service/authentication.service'
import { isAuthorizationHeaderValid } from '../../utils/token-utils'
import { AuthenticatedGuard } from './authenticated.guard'
import { AuthenticatedOrTokenGuard } from './authenticatedOrToken.guard'
import { JwtTokenGuard } from './jwtToken.guard'


jest.mock('./authenticated.guard')
jest.mock('./jwtToken.guard')
jest.mock('../../utils/token-utils')

describe('AuthenticatedOrTokenGuard', () => {
  let authenticatedOrTokenGuard: AuthenticatedOrTokenGuard
  let authenticatedGuard: AuthenticatedGuard
  let jwtTokenGuard: JwtTokenGuard
  let context: ExecutionContext
  let authenticationService: AuthenticationService

  beforeEach(() => {
    authenticatedGuard = new AuthenticatedGuard() as jest.Mocked<AuthenticatedGuard>
    jwtTokenGuard = new JwtTokenGuard(authenticationService) as jest.Mocked<JwtTokenGuard>
    authenticatedOrTokenGuard = new AuthenticatedOrTokenGuard(authenticatedGuard, jwtTokenGuard)

    context = {
      switchToHttp: jest.fn().mockReturnValue({
        getRequest: jest.fn().mockReturnValue({
          headers: {
            authorization: 'Bearer token'
          }
        })
      })
    } as any as ExecutionContext

    ;(authenticatedGuard.canActivate as jest.Mock).mockClear()
    ;(jwtTokenGuard.canActivate as jest.Mock).mockClear()
    ;(isAuthorizationHeaderValid as jest.Mock).mockClear()
  })

  it('should use JwtTokenGuard if the token is present in the authorization header', async () => {
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(true)
    ;(jwtTokenGuard.canActivate as jest.Mock).mockReturnValue(true)

    const result = await authenticatedOrTokenGuard.canActivate(context)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(context.switchToHttp().getRequest())
    expect(jwtTokenGuard.canActivate).toHaveBeenCalledWith(context)
    expect(authenticatedGuard.canActivate).not.toHaveBeenCalled()
    expect(result).toBe(true)
  })

  it('should use AuthenticatedGuard if the token is not present in the authorization header', async () => {
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(false)
    ;(authenticatedGuard.canActivate as jest.Mock).mockReturnValue(true)

    const result = await authenticatedOrTokenGuard.canActivate(context)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(context.switchToHttp().getRequest())
    expect(authenticatedGuard.canActivate).toHaveBeenCalledWith(context)
    expect(jwtTokenGuard.canActivate).not.toHaveBeenCalled()
    expect(result).toBe(true)
  })

  it('should return false if JwtTokenGuard fails', async () => {
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(true)
    ;(jwtTokenGuard.canActivate as jest.Mock).mockReturnValue(false)

    const result = await authenticatedOrTokenGuard.canActivate(context)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(context.switchToHttp().getRequest())
    expect(jwtTokenGuard.canActivate).toHaveBeenCalledWith(context)
    expect(authenticatedGuard.canActivate).not.toHaveBeenCalled()
    expect(result).toBe(false)
  })

  it('should return false if AuthenticatedGuard fails', async () => {
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(false)
    ;(authenticatedGuard.canActivate as jest.Mock).mockReturnValue(false)

    const result = await authenticatedOrTokenGuard.canActivate(context)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(context.switchToHttp().getRequest())
    expect(authenticatedGuard.canActivate).toHaveBeenCalledWith(context)
    expect(jwtTokenGuard.canActivate).not.toHaveBeenCalled()
    expect(result).toBe(false)
  })
})
