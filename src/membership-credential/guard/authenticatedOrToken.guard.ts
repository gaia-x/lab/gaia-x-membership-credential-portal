import { ExecutionContext, Injectable } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'

import { AuthenticatedGuard } from './authenticated.guard'
import { JwtTokenGuard } from './jwtToken.guard'
import {isAuthorizationHeaderValid} from "../../utils/token-utils";

@Injectable()
export class AuthenticatedOrTokenGuard extends AuthGuard('oidc') {
  constructor(
    private readonly authenticatedGuard: AuthenticatedGuard,
    private readonly jwtTokenGuard: JwtTokenGuard
  ) {
    super()
  }

  async canActivate(context: ExecutionContext) {
    const isTokenPresent: boolean = isAuthorizationHeaderValid(context.switchToHttp().getRequest())
    if (isTokenPresent) {
      return this.jwtTokenGuard.canActivate(context)
    }
    return this.authenticatedGuard.canActivate(context)
  }
}
