import { ExecutionContext, UnauthorizedException } from '@nestjs/common'
import { Request } from 'express'

import { AuthenticationService } from '../../auth/service/authentication.service'
import { isAuthorizationHeaderValid, isGaiaXMember } from '../../utils/token-utils'
import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { JwtTokenGuard } from './jwtToken.guard'

jest.mock('../../auth/service/authentication.service')
jest.mock('../../utils/token-utils')

describe('JwtTokenGuard', () => {
  let jwtTokenGuard: JwtTokenGuard
  let authenticationService: jest.Mocked<AuthenticationService>
  let context: jest.Mocked<ExecutionContext>
  let request: jest.Mocked<Request>

  beforeEach(() => {
    authenticationService = {
      authenticate: jest.fn()
    } as any as jest.Mocked<AuthenticationService>

    jwtTokenGuard = new JwtTokenGuard(authenticationService)

    request = {
      header: jest.fn()
    } as any as jest.Mocked<Request>

    context = {
      switchToHttp: jest.fn().mockReturnValue({
        getRequest: jest.fn().mockReturnValue(request)
      })
    } as any as jest.Mocked<ExecutionContext>

    jest.clearAllMocks()
  })

  it('should return true and set user on request if the authorization header is valid and user is authenticated', async () => {
    const user: KeycloakUserDto = {
      name: 'user1',
      sub: '123',
      email: 'email',
      isGaiaXMember: true,
      job_title: 'Software Engineer',
      associated_company_name: 'Gaia-X',
      roles: []
    }
    request.header.mockReturnValue('Bearer valid_token')
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(true)
    ;(isGaiaXMember as jest.Mock).mockReturnValue(true)
    authenticationService.authenticate.mockResolvedValue(user)

    const result = await jwtTokenGuard.canActivate(context)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(request)
    expect(authenticationService.authenticate).toHaveBeenCalledWith('valid_token')
    expect(isGaiaXMember).toHaveBeenCalledWith('valid_token')
    expect(request['user']).toEqual(user)
    expect(result).toBe(true)
  })

  it('should throw UnauthorizedException if the authorization header is invalid', async () => {
    request.header.mockReturnValue('Invalid header')
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(false)

    await expect(jwtTokenGuard.canActivate(context)).rejects.toThrow(UnauthorizedException)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(request)
    expect(authenticationService.authenticate).not.toHaveBeenCalled()
    expect(isGaiaXMember).not.toHaveBeenCalled()
  })

  it('should throw UnauthorizedException if the authorization header is missing', async () => {
    request.header.mockReturnValue(null)
    ;(isAuthorizationHeaderValid as jest.Mock).mockReturnValue(false)

    await expect(jwtTokenGuard.canActivate(context)).rejects.toThrow(UnauthorizedException)

    expect(isAuthorizationHeaderValid).toHaveBeenCalledWith(request)
    expect(authenticationService.authenticate).not.toHaveBeenCalled()
    expect(isGaiaXMember).not.toHaveBeenCalled()
  })
})
