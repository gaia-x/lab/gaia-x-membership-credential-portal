import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common'
import { Request } from 'express'

import { AuthenticationService } from '../../auth/service/authentication.service'
import { extractTokenRoles, isAuthorizationHeaderValid, isGaiaXMember } from '../../utils/token-utils'
import { KeycloakUserDto } from '../dto/keycloak-user.dto'

@Injectable()
export class JwtTokenGuard {
  constructor(private readonly authenticationService: AuthenticationService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest()

    const header = request.header('Authorization')

    if (isAuthorizationHeaderValid(request)) {
      const parts = header.split(' ')
      const token = parts[1]

      const user: KeycloakUserDto = await this.authenticationService.authenticate(token)
      user.isGaiaXMember = isGaiaXMember(token)
      user.roles = extractTokenRoles(token)
      request['user'] = user
      return true
    } else {
      throw new UnauthorizedException('Authorization: Bearer <token> header not present or invalid')
    }
  }
}
