import * as uuid from 'uuid'

import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { MembershipCredentialTypes } from '../dto/membership-credential-types'
import { MembershipCredentialMapper } from './membership-credential.mapper'

jest.mock('uuid')
describe('MembershipCredentialMapper', () => {
  const fakeUuid = '015c51cb-185b-43e1-87f2-5f1cb0ae90ec'
  const fakeUser: KeycloakUserDto = {
    sub: 'user-123',
    name: 'John Doe',
    email: 'john.doe@example.com',
    job_title: 'Software Engineer',
    associated_company_name: 'Gaia-X',
    roles: ['members', 'other-role']
  }
  const fakeDidWeb = 'did:web:example.com'

  beforeAll(() => {
    jest.useFakeTimers()
    jest.setSystemTime(new Date('2024-01-01T00:00:00.000Z'))
    ;(uuid.v4 as jest.Mock).mockImplementation(() => fakeUuid)
  })

  afterAll(() => {
    jest.restoreAllMocks()
    jest.useRealTimers()
  })

  it('should build a valid vcard membership credential', () => {
    const result = MembershipCredentialMapper.buildMembershipCredential(fakeUser, fakeDidWeb, MembershipCredentialTypes.VCARD)

    expect(result).toEqual({
      '@context': ['https://www.w3.org/2018/credentials/v1', 'https://w3id.org/security/suites/jws-2020/v1', 'http://www.w3.org/2006/vcard/ns#'],
      type: ['VerifiableCredential', 'vcard:Organization'],
      id: `did:web:example.com:${fakeUuid}`,
      issuer: 'did:web:example.com',
      issuanceDate: '2024-01-01T00:00:00.000Z',
      credentialSubject: {
        type: 'vcard:Organization',
        'vcard:title': 'Gaia-X European Association for Data and Cloud AISBL',
        'vcard:hasMember': {
          id: fakeUser.sub,
          type: 'vcard:Individual',
          'vcard:fn': fakeUser.name,
          'vcard:hasEmail': fakeUser.email,
          'vcard:title': fakeUser.job_title,
          'vcard:org': fakeUser.associated_company_name,
          'vcard:organization-unit': fakeUser.roles
        }
      }
    })
  })

  it.each([MembershipCredentialTypes.SCHEMA_ORG, undefined])(
    'should build a valid schema.org membership credential %s',
    (type?: MembershipCredentialTypes) => {
      const result = MembershipCredentialMapper.buildMembershipCredential(fakeUser, fakeDidWeb, type)

      expect(result).toEqual({
        '@context': [
          'https://www.w3.org/2018/credentials/v1',
          'https://w3id.org/security/suites/jws-2020/v1',
          'https://schema.org/version/latest/schemaorg-all-https.jsonld'
        ],
        type: ['VerifiableCredential', 'org:ProgramMembership'],
        id: `did:web:example.com:${fakeUuid}`,
        issuer: 'did:web:example.com',
        issuanceDate: '2024-01-01T00:00:00.000Z',
        credentialSubject: {
          type: 'org:ProgramMembership',
          'org:membershipNumber': fakeUser.sub,
          'org:hostingOrganization': 'Gaia-X European Association for Data and Cloud AISBL',
          'org:member': {
            type: 'org:Person',
            'org:name': fakeUser.name,
            'org:email': fakeUser.email,
            'org:jobTitle': fakeUser.job_title,
            'org:worksFor': fakeUser.associated_company_name,
            'org:roleName': fakeUser.roles
          }
        }
      })
    }
  )
})
