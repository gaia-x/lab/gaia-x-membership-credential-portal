import { v4 as uuid } from 'uuid'

import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { MembershipCredentialTypes } from '../dto/membership-credential-types'
import { MembershipCredentialDto } from '../dto/membership-credential.dto'

const GAIAX_LABEL: string = 'Gaia-X European Association for Data and Cloud AISBL'
const VCARD_CONTEXT: string = 'http://www.w3.org/2006/vcard/ns#'
const SCHEMA_ORG_CONTEXT: string = 'https://schema.org/version/latest/schemaorg-all-https.jsonld'
const VCARD_CREDENTIAL_TYPE: string = 'vcard:Organization'
const SCHEMA_ORG_TYPE: string = 'org:ProgramMembership'

export class MembershipCredentialMapper {
  static buildMembershipCredential(user: KeycloakUserDto, didWeb: string, type: MembershipCredentialTypes): MembershipCredentialDto {
    const isVcard: boolean = type === MembershipCredentialTypes.VCARD
    return {
      '@context': [
        'https://www.w3.org/2018/credentials/v1',
        'https://w3id.org/security/suites/jws-2020/v1',
        isVcard ? VCARD_CONTEXT : SCHEMA_ORG_CONTEXT
      ],
      type: ['VerifiableCredential', isVcard ? VCARD_CREDENTIAL_TYPE : SCHEMA_ORG_TYPE],
      id: `${didWeb}:${uuid()}`,
      issuer: didWeb,
      issuanceDate: new Date().toISOString(),
      credentialSubject: isVcard
        ? MembershipCredentialMapper.buildVcardMembershipCredentialSubject(user)
        : MembershipCredentialMapper.buildSchemaOrgMembershipCredentialSubject(user)
    }
  }

  static buildVcardMembershipCredentialSubject(user: KeycloakUserDto) {
    return {
      type: VCARD_CREDENTIAL_TYPE,
      'vcard:title': GAIAX_LABEL,
      'vcard:hasMember': {
        id: user.sub,
        type: 'vcard:Individual',
        'vcard:fn': user.name,
        'vcard:hasEmail': user.email,
        'vcard:title': user.job_title,
        'vcard:org': user.associated_company_name,
        'vcard:organization-unit': user.roles
      }
    }
  }

  static buildSchemaOrgMembershipCredentialSubject(user: KeycloakUserDto) {
    return {
      type: SCHEMA_ORG_TYPE,
      'org:membershipNumber': user.sub,
      'org:hostingOrganization': GAIAX_LABEL,
      'org:member': {
        type: 'org:Person',
        'org:name': user.name,
        'org:email': user.email,
        'org:jobTitle': user.job_title,
        'org:worksFor': user.associated_company_name,
        'org:roleName': user.roles
      }
    }
  }
}
