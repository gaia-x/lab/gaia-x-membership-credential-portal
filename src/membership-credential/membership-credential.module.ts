import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AuthModule } from '../auth/auth.module'
import { MembershipCredentialController } from './controller/membership.controller'
import { AuthenticatedGuard } from './guard/authenticated.guard'
import { AuthenticatedOrTokenGuard } from './guard/authenticatedOrToken.guard'
import { JwtTokenGuard } from './guard/jwtToken.guard'
import { MembershipCredentialService } from './service/membership-credential.service'
import { PrivateKeyService } from './service/private-key.service'

@Module({
  imports: [ConfigModule, AuthModule],
  controllers: [MembershipCredentialController],
  providers: [MembershipCredentialService, PrivateKeyService, AuthenticatedGuard, JwtTokenGuard, AuthenticatedOrTokenGuard]
})
export class MembershipCredentialModule {}
