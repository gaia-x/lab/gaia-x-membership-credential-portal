import { VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { BadRequestException } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import { DateTime } from 'luxon'

import { ConfigServiceMock } from '../../utils/config-service.mock'
import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { MembershipCredentialTypes } from '../dto/membership-credential-types'
import { MembershipCredentialService } from './membership-credential.service'
import { PrivateKeyService } from './private-key.service'


describe('MembershipCredentialService', () => {
  let membershipCredentialService: MembershipCredentialService
  const buildUser = (name?: string, email?: string, sub?: string): KeycloakUserDto => {
    return {
      name,
      email,
      sub,
      isGaiaXMember: true,
      job_title: '',
      associated_company_name: '',
      roles: []
    }
  }

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [MembershipCredentialService, PrivateKeyService]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'https://gaia-x.eu/main',
          PRIVATE_KEY: 'fakePrivateKey',
          PRIVATE_KEY_ALGORITHM: 'fakePrivateKeyAlg',
          OFFLINE_CONTEXTS: 'false'
        })
      )
      .compile()

    membershipCredentialService = moduleRef.get<MembershipCredentialService>(MembershipCredentialService)
  })

  it('should create a signed verifiable credential with type Vcard', async () => {
    const user: KeycloakUserDto = buildUser('John Doe', 'john.doe@gaia-x.eu', '015c51cb-185b-43e1-87f2-5f1cb0ae90ec')
    ;(membershipCredentialService as any).signer = {
      sign: jest.fn(vc => ({
        proof: {},
        ...vc
      }))
    }

    const result: VerifiableCredential = await membershipCredentialService.buildMembershipCredential(user, MembershipCredentialTypes.VCARD)

    expect(result['@context']).toEqual([
      'https://www.w3.org/2018/credentials/v1',
      'https://w3id.org/security/suites/jws-2020/v1',
      'http://www.w3.org/2006/vcard/ns#'
    ])
    expect(result.type).toEqual(['VerifiableCredential', 'vcard:Organization'])
    expect(result.issuer).toEqual('did:web:gaia-x.eu:main')
    expect(DateTime.fromISO(result.issuanceDate).diffNow().milliseconds).toBeLessThan(5000)

    expect(result.credentialSubject.type).toEqual('vcard:Organization')
    expect(result.credentialSubject['vcard:title']).toEqual('Gaia-X European Association for Data and Cloud AISBL')
    expect(result.credentialSubject['vcard:hasMember'].id).toEqual(user.sub)
    expect(result.credentialSubject['vcard:hasMember'].type).toEqual('vcard:Individual')
    expect(result.credentialSubject['vcard:hasMember']['vcard:fn']).toEqual(user.name)
    expect(result.credentialSubject['vcard:hasMember']['vcard:hasEmail']).toEqual(user.email)

    expect(result.proof).toBeTruthy()
  })

  it('should create a signed verifiable credential with type SCHEMA_ORG', async () => {
    const user: KeycloakUserDto = buildUser('John Doe', 'john.doe@gaia-x.eu', '015c51cb-185b-43e1-87f2-5f1cb0ae90ec')
    ;(membershipCredentialService as any).signer = {
      sign: jest.fn(vc => ({
        proof: {},
        ...vc
      }))
    }

    const result: VerifiableCredential = await membershipCredentialService.buildMembershipCredential(user, MembershipCredentialTypes.SCHEMA_ORG)

    expect(result['@context']).toEqual([
      'https://www.w3.org/2018/credentials/v1',
      'https://w3id.org/security/suites/jws-2020/v1',
      'https://schema.org/version/latest/schemaorg-all-https.jsonld'
    ])
    expect(result.type).toEqual(['VerifiableCredential', 'org:ProgramMembership'])
    expect(result.issuer).toEqual('did:web:gaia-x.eu:main')
    expect(DateTime.fromISO(result.issuanceDate).diffNow().milliseconds).toBeLessThan(5000)

    expect(result.credentialSubject.type).toEqual('org:ProgramMembership')
    expect(result.credentialSubject['org:membershipNumber']).toEqual(user.sub)
    expect(result.credentialSubject['org:hostingOrganization']).toEqual('Gaia-X European Association for Data and Cloud AISBL')
    expect(result.credentialSubject['org:member'].type).toEqual('org:Person')
    expect(result.credentialSubject['org:member']['org:name']).toEqual(user.name)
    expect(result.credentialSubject['org:member']['org:email']).toEqual(user.email)

    expect(result.proof).toBeTruthy()
  })

  it.each(['', ' ', undefined, 'someType'])('should throw exception when supplied type is not supported %s', async (type: MembershipCredentialTypes) => {
    const user: KeycloakUserDto = buildUser('John Doe', 'john.doe@gaia-x.eu', '015c51cb-185b-43e1-87f2-5f1cb0ae90ec')
    ;(membershipCredentialService as any).signer = {
      sign: jest.fn(vc => ({
        proof: {},
        ...vc
      }))
    }
    try {
      await membershipCredentialService.buildMembershipCredential(user, type)
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException)
      expect(e.message).toEqual('Unsupported requested type, possible types: VCARD,SCHEMA_ORG')
      return
    }
    throw new Error()
  })

  it.each([buildUser(' ', 'email', 'sub'), buildUser('name', ' ', 'sub'), buildUser('name', 'email', ' '), buildUser()])(
    'should throw an exception if any required attribute is missing %s',
    async (user: KeycloakUserDto) => {
      ;(membershipCredentialService as any).signer = {
        sign: jest.fn(vc => ({
          proof: {},
          ...vc
        }))
      }
      try {
        await membershipCredentialService.buildMembershipCredential(user, MembershipCredentialTypes.SCHEMA_ORG)
      } catch (e) {
        expect(e).toBeInstanceOf(BadRequestException)
        expect(e.message).toEqual('Missing required information for current user')
        return
      }
      throw new Error()
    }
  )
})
