import { JsonWebSignature2020Signer, VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { SignerOptions } from '@gaia-x/json-web-signature-2020/dist/src/options/signer.options'
import { BadRequestException, Injectable, OnModuleInit } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { KeyLike } from 'jose'

import { DidUtils } from '../../utils/did.utils'
import { OfflineDocumentLoader } from '../../utils/jsonld/offline-document.loader'
import { KeycloakUserDto } from '../dto/keycloak-user.dto'
import { MembershipCredentialTypes } from '../dto/membership-credential-types'
import { MembershipCredentialMapper } from '../mapper/membership-credential.mapper'
import { PrivateKeyService } from './private-key.service'


@Injectable()
export class MembershipCredentialService implements OnModuleInit {
  private readonly x509VerificationMethod = 'X509-JWK2020'
  private readonly didWeb: string
  private signer: JsonWebSignature2020Signer

  async onModuleInit() {
    const isOfflineContext = this.configService.get<string>('OFFLINE_CONTEXTS') === 'false'
    const privateKey: KeyLike = await this.privateKeyService.getPrivateKey()
    const signerOptions: SignerOptions = {
      safe: false,
      privateKey,
      privateKeyAlg: this.privateKeyService.getPrivateKeyAlgorithm(),
      verificationMethod: `${this.didWeb}#${this.x509VerificationMethod}`
    }
    if (isOfflineContext) {
      signerOptions.documentLoader = OfflineDocumentLoader.create()
    }
    this.signer = new JsonWebSignature2020Signer(signerOptions)
  }

  constructor(
    private readonly configService: ConfigService,
    private privateKeyService: PrivateKeyService
  ) {
    const baseUrl = this.configService.get<string>('BASE_URL').trim()
    this.didWeb = DidUtils.buildDidWebFromBaseUrl(baseUrl)
  }

  async buildMembershipCredential(user: KeycloakUserDto, type: MembershipCredentialTypes): Promise<VerifiableCredential> {
    if (!this.isUserValid(user)) {
      throw new BadRequestException('Missing required information for current user')
    }

    if (!Object.values(MembershipCredentialTypes).includes(type as MembershipCredentialTypes)) {
      throw new BadRequestException('Unsupported requested type, possible types: ' + Object.values(MembershipCredentialTypes))
    }

      return await this.signer.sign(MembershipCredentialMapper.buildMembershipCredential(user, this.didWeb, type))
  }

  isUserValid = (user: KeycloakUserDto): boolean => {
    return [user.name, user.sub, user.email].every(attr => !!attr && attr.trim() !== '')
  }
}
