import { ConfigService } from '@nestjs/config'
import { KeyObject } from 'crypto'
import { KeyLike } from 'jose'

import { KeyBuilder } from '../../utils/key-builder'
import { PrivateKeyService } from './private-key.service'

describe('PrivateKeyService', () => {
  let privateKeyService: PrivateKeyService
  let configService: ConfigService

  describe('constructor', () => {
    it.each([null, undefined, '', ' '])('should throw an error when private key is missing "%s"', (privateKey: string) => {
      configService = new ConfigService()
      jest.spyOn(configService, 'get').mockImplementation(() => {
        return privateKey
      })

      expect(() => new PrivateKeyService(configService)).toThrow('Please provide a signing private key through the PRIVATE_KEY environment variable')
    })
  })

  describe('getPrivateKey', () => {
    it('should return a private key', async () => {
      const keyPair = KeyBuilder.buildKeyPair()
      configService = new ConfigService()
      jest.spyOn(configService, 'get').mockReturnValue(KeyBuilder.convertToPKCS8(keyPair.privateKey))
      privateKeyService = new PrivateKeyService(configService)

      const privateKey: KeyLike = await privateKeyService.getPrivateKey()

      expect(privateKey).toBeDefined()
      expect(privateKey).toBeInstanceOf(KeyObject)
      expect(privateKey.type).toEqual('private')
    })
  })
})
