import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { KeyLike, importPKCS8 } from 'jose'
import { pki } from 'node-forge'

import { KeyBuilder } from '../../utils/key-builder'

@Injectable()
export class PrivateKeyService {
  constructor(private readonly configService: ConfigService) {
    if (!configService.get<string>('PRIVATE_KEY')?.trim()) {
      throw new Error('Please provide a signing private key through the PRIVATE_KEY environment variable')
    }
  }

  async getPrivateKey(): Promise<KeyLike> {
    return await importPKCS8(this.configService.get<string>('PRIVATE_KEY'), '')
  }

  getPrivateKeyAlgorithm(): string {
    if (this.configService.get<string>('PRIVATE_KEY_ALGORITHM')?.trim()) {
      return this.configService.get<string>('PRIVATE_KEY_ALGORITHM')
    }
    const certificate: pki.Certificate = pki.certificateFromPem(this.configService.get<string>('X509_CERTIFICATE'))
    return KeyBuilder.getPrivateKeyAlgorithm(certificate.siginfo.algorithmOid)
  }
}
