/**
 * A simple {@link ConfigService} mock class.
 * <p>
 *   For example :
 *   ```js
 *   const configServiceMock = new ConfigServiceMock({
 *     MY_PROPERTY: 'testValue',
 *     ANOTHER_PROPERTY: 'anotherValue',
 *   }
 *   ```
 * </p>
 */
export class ConfigServiceMock {
  private readonly properties: Map<string, string>

  constructor(properties: any) {
    this.properties = new Map(Object.entries(properties))
  }

  get(property: string): string {
    const value: string = this.properties.get(property)
    if (!value) {
      throw new Error(`Invalid config property: ${property}`)
    }

    return value
  }
}
