export class DidUtils {
  private static readonly BASE_URL_REGEX = /^\w+:\/\/(.+?)\/*$/

  static buildDidWebFromBaseUrl(baseUrl: string): string {
    const matches = baseUrl.match(DidUtils.BASE_URL_REGEX)
    if (!matches || matches.length != 2) {
      throw new Error(`${baseUrl} doesn't match the required base URL format (ie. https://gaia-x.eu:1234/main)`)
    }

    return 'did:web:' + matches[1].replaceAll('/', ':')
  }
}
