import * as jsonld from 'jsonld'

import { DocumentLoader } from '../../membership-credential/dto/jsonld.types'
import * as credentialContext from '../context/credentials_v1_context.json'
import * as jwsContext from '../context/jws2020_v1_context.json'
import { OfflineDocumentLoader } from './offline-document.loader'


jest.mock('jsonld')

describe('OfflineDocumentLoader', () => {
  it('should resolve offline documents', async () => {
    const mockedDocumentLoader = jest.fn()
    ;(jsonld.documentLoaders.node as jest.Mock).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = OfflineDocumentLoader.create()

    expect(await documentLoader('https://www.w3.org/2018/credentials/v1')).toEqual({
      contextUrl: null,
      document: credentialContext,
      documentUrl: 'https://www.w3.org/2018/credentials/v1'
    })

    expect(await documentLoader('https://w3id.org/security/suites/jws-2020/v1')).toEqual({
      contextUrl: null,
      document: jwsContext,
      documentUrl: 'https://w3id.org/security/suites/jws-2020/v1'
    })

    expect((mockedDocumentLoader as jest.Mock).mock.calls).toHaveLength(0)
  })

  it(`should still call the default document loader when the URL isn't managed`, async () => {
    const mockedDocumentLoader = jest.fn(url => ({
      contextUrl: null,
      document: 'test value',
      documentUrl: url
    }))
    ;(jsonld.documentLoaders.node as jest.Mock).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = OfflineDocumentLoader.create()

    expect(await documentLoader('https://unmanaged.com/shape-registry/shape')).toEqual({
      contextUrl: null,
      document: 'test value',
      documentUrl: 'https://unmanaged.com/shape-registry/shape'
    })

    expect((mockedDocumentLoader as jest.Mock).mock.calls).toHaveLength(1)
    expect((mockedDocumentLoader as jest.Mock).mock.calls[0][0]).toEqual('https://unmanaged.com/shape-registry/shape')
  })
})
