import * as jsonld from 'jsonld'

import { DocumentLoader, RemoteDocument } from '../../membership-credential/dto/jsonld.types'
import * as credentialContext from '../context/credentials_v1_context.json'
import * as jwsContext from '../context/jws2020_v1_context.json'


export class OfflineDocumentLoader {
  private static readonly CONTEXTS: Record<string, any> = {
    'https://www.w3.org/2018/credentials/v1': credentialContext,
    'https://w3id.org/security/suites/jws-2020/v1': jwsContext
  }

  static create(): DocumentLoader {
    const nodeDocumentLoader: DocumentLoader = jsonld.documentLoaders.node()
    return async (url: string): Promise<RemoteDocument> => {
      if (url in OfflineDocumentLoader.CONTEXTS) {
        return {
          contextUrl: null,
          document: OfflineDocumentLoader.CONTEXTS[url],
          documentUrl: url
        }
      }

      return nodeDocumentLoader(url)
    }
  }
}
