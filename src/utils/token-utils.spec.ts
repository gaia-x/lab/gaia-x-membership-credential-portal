import { Request } from 'express'
import * as jose from 'jose'

import { extractTokenRoles, isAuthorizationHeaderValid, isGaiaXMember } from './token-utils'

jest.mock('jose', () => ({
  decodeJwt: jest.fn()
}))

describe('Token Utils', () => {
  describe('isGaiaXMember', () => {
    it('should return true if the accessToken has the roles "members" or "all-staff"', () => {
      ;(jose.decodeJwt as jest.Mock).mockReturnValue({
        realm_access: {
          roles: ['members', 'other-role']
        }
      })

      const result = isGaiaXMember('mockToken')
      expect(result).toBe(true)
    })

    it('should return false if the accessToken does not have the required roles', () => {
      ;(jose.decodeJwt as jest.Mock).mockReturnValue({
        realm_access: {
          roles: ['other-role']
        }
      })

      const result = isGaiaXMember('mockToken')
      expect(result).toBe(false)
    })

    it('should return false if the accessToken does not have realm_access', () => {
      ;(jose.decodeJwt as jest.Mock).mockReturnValue({})

      const result = isGaiaXMember('mockToken')
      expect(result).toBe(false)
    })
  })

  describe('extractTokenRoles', () => {
    it('should return roles in accessToken', () => {
      const roles: string[] = ['members', 'other-role']
      ;(jose.decodeJwt as jest.Mock).mockReturnValue({
        realm_access: {
          roles
        }
      })

      const result = extractTokenRoles('mockToken')
      expect(result).toEqual(roles)
    })

    it('should return an empty array when roles are not defined', () => {
      ;(jose.decodeJwt as jest.Mock).mockReturnValue({})

      const result = extractTokenRoles('mockToken')
      expect(result).toEqual([])
    })
  })

  describe('isAuthorizationHeaderValid', () => {
    it('should return true if the Authorization header is valid', () => {
      const mockRequest = {
        header: jest.fn().mockReturnValue('Bearer validToken')
      } as any as Request

      const result = isAuthorizationHeaderValid(mockRequest)
      expect(result).toBe(true)
    })

    it('should return false if the Authorization header is missing', () => {
      const mockRequest = {
        header: jest.fn().mockReturnValue(null)
      } as any as Request

      const result = isAuthorizationHeaderValid(mockRequest)
      expect(result).toBe(false)
    })

    it('should return false if the Authorization header does not start with Bearer', () => {
      const mockRequest = {
        header: jest.fn().mockReturnValue('Basic invalidToken')
      } as any as Request

      const result = isAuthorizationHeaderValid(mockRequest)
      expect(result).toBe(false)
    })

    it('should return false if the Authorization header is malformed', () => {
      const mockRequest = {
        header: jest.fn().mockReturnValue('Bearer')
      } as any as Request

      const result = isAuthorizationHeaderValid(mockRequest)
      expect(result).toBe(false)
    })

    it('should return false if the Authorization header is not Bearer token', () => {
      const mockRequest = {
        header: jest.fn().mockReturnValue('Bearer validToken extraPart')
      } as any as Request

      const result = isAuthorizationHeaderValid(mockRequest)
      expect(result).toBe(false)
    })
  })
})
