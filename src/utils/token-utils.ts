import { Request } from 'express'
import * as jose from 'jose'
import { JWTPayload } from 'jose'

export const isGaiaXMember = (accessToken: string): boolean => {
  const jwt: JWTPayload = jose.decodeJwt(accessToken)
  return jwt?.realm_access?.['roles'].includes('members', 'all-staff') || false
}

export const isAuthorizationHeaderValid = (request: Request): boolean => {
  const header = request.header('Authorization')
  const parts = header?.split(' ')
  return parts?.length === 2 && parts?.[0] === 'Bearer'
}

export const extractTokenRoles = (accessToken: string): string[] => {
  const jwt: JWTPayload = jose.decodeJwt(accessToken)
  return jwt?.realm_access?.['roles'] || []
}
